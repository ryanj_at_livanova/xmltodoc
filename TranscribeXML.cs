﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Xceed.Words.NET;
using System.Drawing;
using System.Runtime.Remoting.Messaging;
using System.Text.RegularExpressions;

namespace TestXMLToDoc
{
    public class TranscribeXML
    {
        private static string PrettyFormat(string str)
        {
            var strParts = str.Trim()
                .Split('\n')
                .Select(s => s.Trim())
                .ToList();
            for (var i = 1; i < strParts.Count; i++)
            {
                if (strParts[i].FirstOrDefault() == '*' || strParts[i].FirstOrDefault() == '●')
                {
                    if (strParts[i].First() == '*')
                    {
                        strParts[i] = "●" + new string(strParts[i].Skip(1).ToArray());
                    }
                    continue;
                }
                else if (strParts[i - 1].FirstOrDefault() == '●')
                {
                    continue;
                }
                else if (strParts[i] == "")
                {
                    continue;
                }
                else
                {
                    strParts[i - 1] = $"{strParts[i - 1]} {strParts[i]}";
                    strParts.RemoveAt(i);
                    i = 0;
                }
            }

            return string.Join("\n", strParts);
        }

        private static string GetPropertyFormatted(XElement e, string prop)
        {
            var elem = e.Descendants().FirstOrDefault(d => d.Name == prop);
            if (elem == null)
            {
                return "";
            }
            return PrettyFormat(elem.Value);
        }

        public static void ToDoc(string xml)
        {
            var testDict = new Dictionary<string, Dictionary<string, List<(string name, string summary, string acceptanceCrit)>>>();
            var classDict = new Dictionary<string, Dictionary<string, string>>();
            var xmlElements = XDocument.Load(xml).Descendants();

            var tableBorder = new Border(BorderStyle.Tcbs_single, BorderSize.one, 0, Color.Black);

            var nameSpace = xmlElements.First(d => d.Name.LocalName == "assembly").Value;
            
            foreach (var m in xmlElements.Where(d => d.Name.LocalName == "member"))
            {
                string n = m.Attributes().First(a => a.Name == "name").Value;
                n = n.Replace(nameSpace + ".", "");
                var nparts = n.Split(".:".ToCharArray());
                var summary = GetPropertyFormatted(m, "summary");
                var acceptanceCriteria = GetPropertyFormatted(m, "acceptanceCriteria");
                var classname = $"{nparts[1]}";

                switch (nparts[0])
                {
                    case "M":
                        var testname = n.Substring(n.IndexOf(nparts[2]));

                        if (!testDict.ContainsKey(nameSpace))
                        {
                            testDict[nameSpace] = new Dictionary<string, List<(string name, string summary, string acceptanceCrit)>>();
                        }
                        if (!testDict[nameSpace].ContainsKey(classname))
                        {
                            testDict[nameSpace][classname] = new List<(string name, string summary, string acceptanceCrit)>();
                        }
                        testDict[nameSpace][classname].Add((testname, summary, acceptanceCriteria));
                        break;
                    case "T":
                        if (!classDict.ContainsKey(nameSpace))
                        {
                            classDict[nameSpace] = new Dictionary<string, string>();
                        }
                        classDict[nameSpace][classname] = summary;
                        break;
                    default:
                        break;
                }

            }


            foreach (var n in testDict.Keys)
            {
                using (var doc = DocX.Create($"{n}_Descriptions.docx"))
                {
                    foreach (var c in testDict[n].Keys)
                    {
                        var p = doc.InsertParagraph($"{c}");
                        p.Heading(HeadingType.Heading2);
                        if (classDict.ContainsKey(c))
                        {
                            doc.InsertParagraph(classDict[n][c]);
                        }

                        var instr = doc.InsertParagraph("\nTo run tests in this class, execute:");
                        var table = doc.AddTable(1, 1);
                        table
                            .Rows[0]
                            .Cells[0]
                            .Paragraphs[0]
                            .Append(
                                $@"""C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\CommonExtensions\Microsoft\TestWindow\vstest.console.exe""
""{nameSpace}.dll"" 
/TestCaseFilter:""ClassName={c}""
/Logger:trx")
                            .Font("Courier New")
                            .FontSize(10);
                        table.SetBorder(TableBorderType.Bottom, tableBorder);
                        table.SetBorder(TableBorderType.Top, tableBorder);
                        table.SetBorder(TableBorderType.Left, tableBorder);
                        table.SetBorder(TableBorderType.Right, tableBorder);
                        instr.InsertTableAfterSelf(table);

                        var testPara = doc.InsertParagraph("\n");

                        var testTable = doc.AddTable(testDict[n][c].Count + 1, 3);
                        testTable.Rows[0].Cells[0].Paragraphs[0].Append("Test Name").Bold();
                        testTable.Rows[0].Cells[1].Paragraphs[0].Append("Description").Bold();
                        testTable.Rows[0].Cells[2].Paragraphs[0].Append("Acceptance Criteria").Bold();
                        var r = 1;
                        foreach (var t in testDict[n][c])
                        {
                            testTable.Rows[r].Cells[0].Paragraphs[0].Append(string.Join("_\n", t.name.Split('_')));
                            testTable.Rows[r].Cells[1].Paragraphs[0].Append(t.summary);
                            testTable.Rows[r].Cells[2].Paragraphs[0].Append(t.acceptanceCrit);
                            r++;
                        }

                        testTable.SetBorder(TableBorderType.Bottom, tableBorder);
                        testTable.SetBorder(TableBorderType.Top, tableBorder);
                        testTable.SetBorder(TableBorderType.Left, tableBorder);
                        testTable.SetBorder(TableBorderType.Right, tableBorder);
                        testTable.SetBorder(TableBorderType.InsideH, tableBorder);
                        testTable.SetBorder(TableBorderType.InsideV, tableBorder);
                        testPara.InsertTableAfterSelf(testTable);
                    }

                    doc.Save();
                }
            }
        }
    }
}