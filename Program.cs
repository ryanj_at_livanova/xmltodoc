﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Words.NET;
using System.Windows;
using System.Windows.Forms;

namespace TestXMLToDoc
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Program must be run with --path option");
                return;
            }

            string path = "";
            for (var i = 0; i < args.Length; i++)
            {
                if (args[i] == "--path")
                {
                    path = args[++i];
                }
            }

            foreach (var file in Directory.GetFiles(path, "*.xml"))
            {
                if (!File.Exists($"{file}"))
                {
                    Console.WriteLine($"File {file} does not exist");
                }

                TranscribeXML.ToDoc($"{file}");
            }
        }
    }
}